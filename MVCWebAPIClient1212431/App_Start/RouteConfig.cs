﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCWebAPIClient1212431
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Index",
                url: "",
                defaults: new { controller = "Home", action = "ViewAllUsers1212431" }
            );

            routes.MapRoute(
                name: "ViewAllUsers",
                url: "Home/ViewAllUsers",
                defaults: new { controller = "Home", action = "ViewAllUsers1212431" }
            );

            routes.MapRoute(
                name: "ViewUser",
                url: "Home/ViewUser",
                defaults: new { controller = "Home", action = "ViewUser1212431" }
            );

            routes.MapRoute(
                name: "AddUser",
                url: "Home/AddUser",
                defaults: new { controller = "Home", action = "AddUser1212431" }
            );

            routes.MapRoute(
                name: "EditUser",
                url: "Home/EditUser",
                defaults: new { controller = "Home", action = "EditUser1212431" }
            );

            routes.MapRoute(
                name: "DeleteUser",
                url: "Home/DeleteUser",
                defaults: new { controller = "Home", action = "DeleteUser1212431" }
            );

            routes.MapRoute(
                name: "ViewProductByCategory",
                url: "Home/ViewProductByCategory",
                defaults: new { controller = "Home", action = "ViewProductByCategory1212431" }
            );

            routes.MapRoute(
                name: "ViewProductByCategoryAndUser",
                url: "Home/ViewProductByCategoryAndUser",
                defaults: new { controller = "Home", action = "ViewProductByCategoryAndUser1212431" }
            );
        }
    }
}