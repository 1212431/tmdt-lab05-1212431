﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCWebAPIClient1212431.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MVCWebAPIClient1212431.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {

            return View();
        }

        [HttpGet]
        public ActionResult ViewAllUsers1212431()
        {
            List<User> userList = new List<User>();
            return View(userList);
        }

        [HttpPost]
        public ActionResult ViewAllUsers1212431(string hostName, string apiLink)
        {
            List<User> userList = new List<User>();
            if (!String.IsNullOrEmpty(hostName) && !String.IsNullOrEmpty(apiLink))
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(hostName);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = client.GetAsync(apiLink).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            userList = response.Content.ReadAsAsync<List<User>>().Result;
                        }
                    }
                }
                catch (Exception e)
                { }

            }

            return View(userList);
        }

        [HttpGet]
        public ActionResult ViewUser1212431()
        {
            User user = null;
            return View(user);
        }
        [HttpPost]
        public ActionResult ViewUser1212431(string hostName, string apiLink)
        {
            User user = new User();
            if (!String.IsNullOrEmpty(hostName) && !String.IsNullOrEmpty(apiLink))
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(hostName);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = client.GetAsync(apiLink).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            user = response.Content.ReadAsAsync<User>().Result;
                        }
                    }
                }
                catch (Exception e)
                {
                    user = null;
                }

            }
            else
            {
                user = null;
            }
            return View(user);
        }


        [HttpGet]
        public ActionResult AddUser1212431()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddUser1212431(FormCollection formCollection)
        {
            User user = new User();
            Result result = new Result();
            string hostName = formCollection["hostName"];
            string apiLink = formCollection["apiLink"] + "?token=" + formCollection["token"];
            if (!String.IsNullOrEmpty(hostName) && !String.IsNullOrEmpty(apiLink))
            {
                user.Username = formCollection["Username"];
                user.Fullname = formCollection["Fullname"];
                user.Phone = formCollection["Phone"];
                user.Email = formCollection["Email"];

                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(hostName);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = client.PostAsJsonAsync(apiLink, user).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            result = response.Content.ReadAsAsync<Result>().Result;
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Status = "Fail";
                }

            }
            else
            {
                result = null;
            }
            return View(result);
        }


        [HttpGet]
        public ActionResult EditUser1212431()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EditUser1212431(FormCollection formCollection)
        {
            User user = new User();
            Result result = new Result();
            string hostName = formCollection["hostName"];
            string apiLink = formCollection["apiLink"] + "?token=" + formCollection["token"];
            if (!String.IsNullOrEmpty(hostName) && !String.IsNullOrEmpty(apiLink))
            {
                user.Username = formCollection["Username"];
                user.Fullname = formCollection["Fullname"];
                user.Phone = formCollection["Phone"];
                user.Email = formCollection["Email"];

                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(hostName);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = client.PutAsJsonAsync(apiLink, user).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            result = response.Content.ReadAsAsync<Result>().Result;
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Status = "Fail";
                }

            }
            else
            {
                result = null;
            }
            return View(result);
        }


        [HttpGet]
        public ActionResult DeleteUser1212431()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DeleteUser1212431(FormCollection formCollection)
        {
            Result result = new Result();
            string hostName = formCollection["hostName"];
            string apiLink = formCollection["apiLink"] + "?token=" + formCollection["token"];
            if (!String.IsNullOrEmpty(hostName) && !String.IsNullOrEmpty(apiLink))
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(hostName);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = client.DeleteAsync(apiLink).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            result = response.Content.ReadAsAsync<Result>().Result;
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Status = "Fail";
                }

            }
            else
            {
                result = null;
            }
            return View(result);
        }


        [HttpGet]
        public ActionResult ViewProductByCategory1212431()
        {
            List<Product> productList = new List<Product>();
            return View(productList);
        }

        [HttpPost]
        public ActionResult ViewProductByCategory1212431(FormCollection formCollection)
        {
            List<Product> productList = new List<Product>();
            string hostName = formCollection["hostName"];
            string apiLink = formCollection["apiLink"];
            if (!String.IsNullOrEmpty(hostName) && !String.IsNullOrEmpty(apiLink))
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(hostName);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = client.GetAsync(apiLink).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            productList = response.Content.ReadAsAsync<List<Product>>().Result;
                        }
                    }
                }
                catch (Exception e)
                { }

            }


            return View(productList);
        }

        [HttpGet]
        public ActionResult ViewProductByCategoryAndUser1212431()
        {
            List<Product> productList = new List<Product>();
            return View(productList);
        }

        [HttpPost]
        public ActionResult ViewProductByCategoryAndUser1212431(FormCollection formCollection)
        {
            List<Product> productList = new List<Product>();
            string hostName = formCollection["hostName"];
            string apiLink = formCollection["apiLink"];
            if (!String.IsNullOrEmpty(hostName) && !String.IsNullOrEmpty(apiLink))
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(hostName);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = client.GetAsync(apiLink).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            productList = response.Content.ReadAsAsync<List<Product>>().Result;
                        }
                    }
                }
                catch (Exception e)
                { }

            }

            return View(productList);
        }
    }
}
