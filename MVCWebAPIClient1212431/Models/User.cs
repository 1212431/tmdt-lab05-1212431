﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebAPIClient1212431.Models
{
    public class User
    {
        public String Username { get; set; }
        public String Fullname { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public String Password { get; set; }
    }
}