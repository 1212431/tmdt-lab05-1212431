﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebAPI1212431
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "UserApi",
                routeTemplate: "api/2.0/users/{username}",
                defaults: new { controller = "users", username = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ProductApi",
                routeTemplate: "api/2.0/products/",
                defaults: new { controller = "products" }
            );
            config.Routes.MapHttpRoute(
                name: "ProductUserApi",
                routeTemplate: "api/2.0/products/{category}/{username}",
                defaults: new { controller = "products" }
            );

        }
    }
}
