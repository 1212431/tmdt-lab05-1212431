﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212431.Models;

namespace WebAPI1212431.Controllers
{
    public class ProductsController : ApiController
    {
        static readonly IProductRepository repository = new ProductRepository();

        /*
         * GET /api/2.0/products?category=categoryValue
         */
        public IHttpActionResult GetProductsByCategory1212431([FromUri]string Category)
        {
            IEnumerable<Product> productLists = repository.GetAll().Where(
                p => string.Equals(p.Category, Category, StringComparison.OrdinalIgnoreCase));
            return Ok(productLists);
        }

        /*
         * GET /api/2.0/products/{category}/{username}
         */
        public IHttpActionResult GetProductByCategoryUsername1212431(string Category, string Username)
        {
            IEnumerable<Product> productLists = repository.GetAll().Where(
                p => string.Equals(p.Category, Category, StringComparison.OrdinalIgnoreCase))
                .Where(p => string.Equals(p.Username, Username, StringComparison.OrdinalIgnoreCase));
            return Ok(productLists);
        }

    }
}
