﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAPI1212431.Models;

namespace WebAPI1212431.Controllers
{
    public class TokenController : Controller
    {
        static readonly IUserRepository userRepository = new UserRepository();
        
        //
        // GET: /Token/
        [HttpGet]
        public ActionResult Index1212431()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index1212431(string Username, string Password)
        {
            string token = "";
            bool login = false;
            if (!String.IsNullOrEmpty(Username) || !String.IsNullOrEmpty(Password))
            {
                login = userRepository.Login(Username, Password);

                if (login)
                {
                    token = UsersController.tokenRepository.GenerateToken(Username);
                    ViewBag.Message = "Tạo mới access token thành công!";
                }
                else
                {
                    ViewBag.Message = "Tên đăng nhập hoặc mật khẩu không đúng!";
                }
            }
            return View((object)token);
        }
	}
}