﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212431.Models;

namespace WebAPI1212431.Controllers
{
    public class UsersController : ApiController
    {
        static readonly IUserRepository userRepository = new UserRepository();
        public static readonly IAccessTokenRepository tokenRepository = new AccessTokenRepository();


        /*
         *  Get /api/2.0/users
         */
        public IHttpActionResult GetAllUsers1212431()
        {
            IEnumerable<User> userList = userRepository.GetAll();
            return Ok(userList);
        }

        /*
         * Get /api/2.0/users/{username}
         */
        public IHttpActionResult GetUser1212431(String username)
        {
            User item = userRepository.Get(username);
            if (item == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(item);
            }
            
        }

        /*
         * POST /api/2.0/users/
         */
        public IHttpActionResult PostUser1212431(User item, String token)
        {
            if (!tokenRepository.AuthToken(token))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            item = userRepository.Add(item);
            Result result = new Result();
            if (item != null)
            {
                result.Status = "Ok";
            }
            else
            {
                result.Status = "Fail";
            }

            return Ok(result);
        }

        /*
         * PUT /api/2.0/users/{username}
         */
        public IHttpActionResult PutUser1212431(string username, User item, string token)
        {
            if (!tokenRepository.AuthToken(token))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            Result result = new Result();
            if (userRepository.Update(username, item))
            {
                result.Status = "Ok";
            }
            else
            {
                result.Status = "Fail";
            }
            return Ok(result);
        }

        /*
         * DELETE /api/2.0/users/{username}
         */
        public IHttpActionResult DeleteUser1212431(string username, string token)
        {
            if (!tokenRepository.AuthToken(token))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            User item = userRepository.Get(username);
            Result result = new Result();
            if (item == null)
            {
                result.Status = "Fail";
            }
            else
            {
                userRepository.Remove(username);
                result.Status = "Ok";
                
            }
            return Ok(result);
        }
    }
}
