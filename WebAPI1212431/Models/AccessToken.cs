﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212431.Models
{
    public class AccessToken
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}