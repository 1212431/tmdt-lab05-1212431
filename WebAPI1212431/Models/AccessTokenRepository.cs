﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212431.Models
{
    public class AccessTokenRepository: IAccessTokenRepository
    {
        private List<AccessToken> tokens = new List<AccessToken>();

        public AccessTokenRepository()
        {

        }

        public string GenerateToken(string Username)
        {
            string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()) + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Username));
            int listSize = tokens.Count;
            for (int i = 0; i < listSize; i++ )
            {
                if (tokens[i].Username == Username)
                {
                    tokens[i].Token = token;
                    return token;
                }
            }

            tokens.Add(new AccessToken { Username = Username, Token = token});
            return token;
        }
        public bool AuthToken(string Token)
        {
            bool isAuthenticated = false;
            int listSize = tokens.Count;
            for (int i = 0; i < listSize; i++)
            {
                if (tokens[i].Token == Token)
                {
                    isAuthenticated = true;
                    break;
                }
            }
            return isAuthenticated;
        }
        public string GetToken(string Username)
        {
            string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            int listSize = tokens.Count;
            for (int i = 0; i < listSize; i++)
            {
                if (tokens[i].Username == Username)
                {

                    return tokens[i].Token;
                }
            }
            return null;
        }
    }
}