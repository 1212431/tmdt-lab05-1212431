﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212431.Models
{
    public interface IAccessTokenRepository
    {
        string GenerateToken(string Username);
        bool AuthToken(string Token);
        string GetToken(string Username);
    }
}
