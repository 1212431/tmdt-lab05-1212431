﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212431.Models
{
    interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User Get(String username);
        User Add(User item);
        void Remove(String Username);
        bool Update(String Username, User item);
        bool Login(String Username, String Password);
    }
}
