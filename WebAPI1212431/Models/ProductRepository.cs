﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212431.Models
{
    public class ProductRepository:IProductRepository
    {
        private List<Product> products = new List<Product>();
        private int _nextId = 1;

        public ProductRepository()
        {
            Add(new Product { Name = "iPhone 6s", Category = "Smartphone", Price = 699, Username = "admin" });
            Add(new Product { Name = "iPhone 6s+", Category = "Smartphone", Price = 899, Username = "admin" });
            Add(new Product { Name = "iPhone 5s", Category = "Smartphone", Price = 499, Username = "1212431" });
            Add(new Product { Name = "Nokia 1280", Category = "Stupidphone", Price = 29, Username = "1212431" });
            Add(new Product { Name = "New Macbook Air 11-inch MJVM2", Category = "Laptop", Price = 899, Username = "admin" });
            Add(new Product { Name = "New Macbook Air 13-inch MJVE2", Category = "Laptop", Price = 999, Username = "1212431" });
            Add(new Product { Name = "New Macbook Pro Retina 15-inch - MJLT2", Category = "Laptop", Price = 2499, Username = "1212431" });
        }

        public IEnumerable<Product> GetAll()
        {
            return products;
        }

        public Product Get(int id)
        {
            return products.Find(p => p.Id == id);
        }

        public Product Add(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            products.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            products.RemoveAll(p => p.Id == id);
        }

        public bool Update(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = products.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            products.RemoveAt(index);
            products.Add(item);
            return true;
        }
    }
}