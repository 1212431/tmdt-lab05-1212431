﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212431.Models
{
    public class UserRepository: IUserRepository
    {
        private List<User> users = new List<User>();
        
        public UserRepository()
        {
            Add(new User { Username = "1212431",
                        Email = "1212431@student.hcmus.edu.vn",
                        Fullname = "Nguyễn Nguyên Trà",
                        Phone = "0123456789",
                        Password = "1212431"
            });
            Add(new User
            {
                Username = "admin",
                Email = "admin@student.hcmus.edu.vn",
                Fullname = "Admin",
                Phone = "0123456789",
                Password = "123456"
            });
            Add(new User
            {
                Username = "guest",
                Email = "guest@student.hcmus.edu.vn",
                Fullname = "Khách",
                Phone = "0123456789",
                Password = "123456"
            });
        }
        public IEnumerable<User> GetAll()
        {
            return users;
        }
        public User Get(String Username)
        {
            int userCount = users.Count;
            // Tìm xem user name đã có trong csdl chưa, nếu chưa có thì return null
            for (int i = 0; i < userCount; i++)
            {
                if (users[i].Username == Username)
                {
                    return users[i];
                }
            }
            return null;
        }

        public User Add(User item)
        {
            int userCount = users.Count;
            
            // Kiểm tra nếu đã tồn tại username trong hệ thống thì return null.
            for (int i = 0; i < userCount; i++)
            {
                if (users[i].Username == item.Username)
                {
                    return null;
                }
            }

            users.Add(item);
            return item;
        }
        public void Remove(String Username)
        {
            int userCount = users.Count;

            // Kiểm tra nếu đã tồn tại username trong hệ thống thì remove và kết thúc.
            for (int i = 0; i < userCount; i++)
            {
                if (users[i].Username == Username)
                {
                    users.RemoveAt(i);
                    return;
                }
            }
        }

        public bool Update(String Username, User item)
        {
            int userCount = users.Count;

            // Kiểm tra nếu đã tồn tại username trong csdl thì update.
            for (int i = 0; i < userCount; i++)
            {
                if (users[i].Username == Username)
                {
                    users[i] = item;
                    return true;
                }
            }
            return false;
        }

        public bool Login(String Username, String Password)
        {
            int userCount = users.Count;
            bool login = false;
            for (int i = 0; i < userCount; i++)
            {
                if (users[i].Username == Username)
                {
                    if (users[i].Password == Password)
                    {
                        login = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return login;
        }
    }
}